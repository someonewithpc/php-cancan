<?php declare(strict_types = 1);

namespace CanCan;

class Ability {

    /**
     * @var array<string, array<string, array{base: bool, conditions: array<string, mixed>, properties: array<int, string>, checker: callable}>> $abilities
     */
    private static array $abilities = [];

    /**
     * @template T
     * @param array<T> $in
     * @param callable(T, array<T>, array-key): bool $condition
     * @return array{array<T>, array<T>}
     */
    public static function partition(array $in, callable $condition): array {
        $matches = [];
        $not_matches = [];
        foreach ($in as $k => $v) {
            if ($condition($v, $in, $k)) {
                if (\is_string($k)) $matches[$k] = $v;
                else $matches[] = $v;
            } else {
                if (\is_string($k)) $not_matches[$k] = $v;
                else $not_matches[] = $v;
            }
        }
        return [$matches, $not_matches];
    }

    /**
     * @param array<mixed> $properties_and_conditions
     */
    private static function base_allow(bool $base, string $action, string $subject_class, array $properties_and_conditions, callable|null $checker): void {
        self::$abilities[$action] = self::$abilities[$action] ?? [];
        self::$abilities[$action][$subject_class] = self::$abilities[$action][$subject_class] ?? [];

        [$conditions, $properties] = self::partition($properties_and_conditions, fn($_, $__, $k) => \is_string($k));
        self::$abilities[$action][$subject_class][] = [
            'base' => $base,
            'conditions' => $conditions,
            'properties' => $properties,
            'checker' => $checker,
        ];
    }

    /**
     * Like CanCan's `can`
     * @param array<mixed> $properties_and_conditions
     */
    public static function allow(string $action, string $subject_class, array $properties_and_conditions = [], callable|null $checker = null): void {
        self::base_allow(true, $action, $subject_class, $properties_and_conditions, $checker);
    }

    /**
     * Like CanCan's `can`
     * @param array<mixed> $properties_and_conditions
     */
    public static function disallow(string $action, string $subject_class, array $properties_and_conditions = [], callable|null $checker = null): void {
        self::base_allow(false, $action, $subject_class, $properties_and_conditions, $checker);
    }

    /**
     * Like CanCan's `can?`, but this is not a valid name in PHP
     *
     * TODO handle properties
     *
     * @param array<int, mixed> $properties
     * @param mixed[] $extra_callback_args
     */
    public static function can(string $action, string|object $subject, array $properties = [], array $extra_callback_args = []): bool {
        $action_abilities = self::$abilities[$action];
        if (!\count($action_abilities)) return false;

        $class = \is_object($subject) ? $subject::class : $subject;
        $abilities = $action_abilities[$class];
        if (!\count($abilities)) return false;
        else {
            foreach ($abilities as $a) {
                if (!\is_null($a['checker'])) {
                    if ($a['checker']($subject, ...$properties, ...$extra_callback_args)) {
                        return $a['base'];
                    }
                } else if (\count($a['conditions']) && \is_object($subject)) {
                    $all = true;
                    foreach ($a['conditions'] as $prop => $value) {
                        $all = $all && \property_exists($subject, $prop) && $subject->{$prop} === $value;
                    }
                    if ($all) {
                        return $a['base'];
                    }
                } else {
                    return $a['base'];
                }
            }
        }
        return false;
    }

    public static function cannot(...$args): bool {
        return !self::can(...$args);
    }

    public static function resetAll(): void {
        self::$abilities = [];
    }
}
