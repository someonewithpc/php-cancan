<?php declare(strict_types = 1);

namespace CanCan;

function dd(mixed ...$args): void { var_dump(...$args); exit(); }

class BeforeActionWrapper {

    /**
     * @param class-name|object|callable $wrapped - a string class name, an object or a [string|object, method] callable
     */
    public function __construct(private mixed $wrapped) {}

    /**
     * Read $wrapped's $class `before_actions` and run them before forwarding the
     * call through to the controller, possibly loading and setting resources
     * from the URL parameters into instance varaibles
     *
     * `before_actions` should be an array of actions, where each has:
     *  - 0 => one of:
     *      * callable - A function to execute before the controller action
     *      * string   - A model class name where `::find` gets called\
     *      * array:
     *        = find_by - An array of keys to use as properties to find the resource by
     *
     *  - only => only consider this action if the called $method is contained therein
     *  - except => consider this action except if the called $method is contained therein
     *  - instance_name | instance_property => if present, use this name to as the name of the property
     *            into which to load the resource, otherwise use the $class with 'Entity' removed and converted to snake_case
     */
    public function __call(string $method, array $args) {
        if (\is_object($this->wrapped)) {
            $object = $this->wrapped;
            $class = \get_class($this->wrapped);
            $target = [$this->wrapped, $method];
        } else if (\is_string($this->wrapped)) {
            $class = $this->wrapped;
            $object = new $class;
            $target = [$object, $method];
        } else if (\is_array($this->wrapped) && \count($this->wrapped) === 2) {
            if (\is_string($this->wrapped[0])) {
                $class = $this->wrapped[0];
                $object = new $class;
                $this->wrapped[0] = $object;
            } else {
                $class = \get_class($this->wrapped[0]);
                $object = $this->wrapped[0];
            }
            $args[] = $method;
            $target = $this->wrapped;
        }

        if (defined("${class}::before_actions")) {
            // TODO extract params
            $params = []; // GET parameters
            foreach ($class::before_actions as $action) {
                if (isset($action['only']) && !\in_array($method, (array) $action['only'])) continue;
                if (isset($action['except']) && \in_array($method, (array) $action['except'])) continue;

                $instance_property = $action['instance_name'] ?? $action['instance_property'] ?? self::camelCaseToSnakeCase(str_replace('Entity', '', $class));

                if (\is_callable($action[0]) || (\is_array($action[0]) && \count($action[0]) === 2)) {
                    if ($action[0][0] === $class && !\is_null($object)) {
                        $action[0][0] = $object;
                    }
                    [$object, $loader_method] = $action[0];
                    $this->callPrivate($object, $loader_method, $params);
                } else if (\is_string($action[0])) {
                    $object->find($params);
                } else if (\is_array($action[0]) && isset($action[0]['find_by'])) {
                    $object->findBy(\array_combine($action[0]['find_by'], $params));
                }
            }
        }

        $target(...$args);
    }

    private static function camelCaseToSnakeCase(string $str): string
    {
        return mb_strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $str));
    }

    private function callPrivate(object $obj, string $method, array $args): mixed {
        $closure = function () use ($method) { return $this->{$method}(...func_get_args()); };
        return $closure->call($obj, $args);
    }
}
