<?php declare(strict_types=1);

use CanCan\Ability;
use PHPUnit\Framework\TestCase;

class A {
    public function __construct(public bool $prop) {}
}

class AbilityTest extends TestCase {
    protected function tearDown(): void {
        Ability::resetAll();
    }

    public function testPartition(): void {
        $this->assertEquals(
            [['key' => 'val'], []],
            Ability::partition(['key' => 'val'], fn($_, $__, $k) => \is_string($k)),
        );
        $this->assertEquals(
            [['key' => 'val', 'key2' => 'val2'], []],
            Ability::partition(['key' => 'val', 'key2' => 'val2'], fn($_, $__, $k) => \is_string($k)),
        );

        $this->assertEquals(
            [['number'], []],
            Ability::partition(['number'], fn($_, $__, $k) => \is_int($k)),
        );

        $this->assertEquals(
            [[], ['number']],
            Ability::partition(['number'], fn($_, $__, $k) => \is_string($k)),
        );

        $this->assertEquals(
            [[1, 2, 3], [4, 5]],
            Ability::partition([1, 2, 3, 4, 5], fn ($v) => $v <= 3),
        );
    }

    public function testAllow(): void {
        Ability::allow('create', A::class);
        $this->assertTrue(Ability::can('create', A::class));
        $this->assertFalse(Ability::cannot('create', A::class));
    }

    public function testDisallow(): void {
        Ability::disallow('create', A::class);
        $this->assertFalse(Ability::can('create', A::class));
        $this->assertTrue(Ability::cannot('create', A::class));
    }

    public function testAllowCondition(): void {
        Ability::allow('read', A::class, ['prop' => true]);
        $this->assertTrue(Ability::can('read', new A(prop: true)));
        $this->assertFalse(Ability::can('read', new A(prop: false)));
    }

    public function testAllowCallBack(): void {
        Ability::allow('read', A::class, checker: fn (A $obj) => $obj->prop === true);
        $this->assertTrue(Ability::can('read', new A(prop: true)));
        $this->assertFalse(Ability::can('read', new A(prop: false)));
    }
}
