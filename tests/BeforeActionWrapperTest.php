<?php  declare(strict_types = 1);

namespace Test;

use CanCan\BeforeActionWrapper;
use PHPUnit\Framework\TestCase;

class NoFilter {
    public function objAndMethod(BeforeActionWrapperTest $test_case, string $method): void {
        $test_case->assertEquals('method1', $method);
    }

    public function onlyObject(BeforeActionWrapperTest $test_case): void {
        $test_case->assertTrue(true);
    }

    public static function classAndMethod(BeforeActionWrapperTest $test_case, string $method): void {
        $test_case->assertEquals('method2', $method);
    }

    public function onlyClass(BeforeActionWrapperTest $test_case): void {
        $test_case->assertTrue(true);
    }

    public static function onlyClassStatic(BeforeActionWrapperTest $test_case): void {
        $test_case->assertTrue(true);
    }
}

class FilterCallable {
    public const before_actions = [
        [
            [self::class, 'load'],
        ]
    ];

    public function onlyObjectCallable(BeforeActionWrapperTest $test_case) {
        $test_case->assertNotNull($this->var);
    }

    private function load() {
        $this->var = new \stdClass;
    }
}

class BeforeActionWrapperTest extends TestCase {
    public function testWrapNoFilter(): void {
        (new BeforeActionWrapper([new NoFilter, 'objAndMethod']))->method1($this);
        (new BeforeActionWrapper(new NoFilter))->onlyObject($this);
        (new BeforeActionWrapper([NoFilter::class, 'classAndMethod']))->method2($this);
        (new BeforeActionWrapper(NoFilter::class))->onlyClass($this);
        (new BeforeActionWrapper(NoFilter::class))->onlyClassStatic($this);
    }

    public function testWrapFilter(): void {
        (new BeforeActionWrapper(new FilterCallable))->onlyObjectCallable($this);
    }
}
